import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UsuarioModel } from '../models/user.model';

//rxjs
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  usuario: UsuarioModel;
  base_url: string;

  constructor( private http: HttpClient ) { 
    this.usuario = new UsuarioModel();
    this.base_url = environment.URI_API;
  }
  getAuth(user: UsuarioModel){
    const userLogin = {
      ...user
    }
    return this.http.post(`${this.base_url}/login`, userLogin)
            .pipe(
              map(
                res => {
                  this.setToken(res['token']);
                  return res
                }
              )
            )
  }
  
  verifyAuth(): boolean{
    if(localStorage.getItem('Token')){
      return true;
    } else {
      return false;
    }
  }

  private setToken(token: string): void {
    const expira:number = new Date().getTime() + 3600;
    localStorage.setItem('Token', token);
    localStorage.setItem('Expira', expira.toString());
  }
}
