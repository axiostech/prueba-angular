import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-how-work',
  templateUrl: './how-work.component.html',
  styleUrls: ['./how-work.component.css']
})
export class HowWorkComponent implements OnInit {

  title: string;
  parag: string;

  constructor() { 
    this.title = 'How It Works';
    this.parag = 'Because finding a good pet sitter shouldn’t be a hassle. With Fetch! It’s as easy as…';
  }

  ngOnInit(): void {
  }

}
