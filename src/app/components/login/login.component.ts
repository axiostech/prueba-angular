import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

//models
import { UsuarioModel } from 'src/app/models/user.model';

// services
import { AuthService } from 'src/app/services/auth.service';

//libs
import Swal from 'sweetalert2'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css'
  ]
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;
  loading:boolean = false;
  
  constructor(
    private auth: AuthService,
    private router: Router
  ) { 
  }

  ngOnInit(): void {
    this.usuario = new UsuarioModel();
  }

  onSubmit(form: NgForm){
    if (form.invalid) {
      return
    };
    this.loading = true;
    this.auth.getAuth(this.usuario)
      .subscribe( response => {
        this.loading = false;
        console.log(response);
        this.router.navigate(['home']);
      }, err => {
        this.loading = false;
        Swal.fire({
          title: 'Error!',
          text: err.error.error,
          icon: 'error',
          confirmButtonText: 'Aceptar'
        })
      })
  }
}