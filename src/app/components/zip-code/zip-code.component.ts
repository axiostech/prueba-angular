import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-zip-code',
  templateUrl: './zip-code.component.html',
  styleUrls: ['./zip-code.component.css']
})
export class ZipCodeComponent implements OnInit {
  
  @Input() title: string;
  @Input() parag: string;

  constructor() { }

  ngOnInit(): void {
  }

}
