import { Component, OnInit } from '@angular/core';
import { Swiper, SwiperOptions, Pagination, Navigation } from 'swiper';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  swiper;

  swiperParams: SwiperOptions = {
    slidesPerView: 3.5,
    loop: true,
    spaceBetween: 10,
    centeredSlides: true,
    flipEffect: {
      slideShadows: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
  };

  constructor() { 
  }

  ngOnInit(): void {
    this.swiper = new Swiper('.swiper-container', this.swiperParams);
  }

}
