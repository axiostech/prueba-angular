import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.css']
})
export class OurServicesComponent implements OnInit {

  title: string;
  parag: string;

  constructor() { 
    this.title = 'How It Works';
    this.parag = 'Because finding a good pet sitter shouldn’t be a hassle. With Fetch! It’s as easy as…';
  }


  ngOnInit(): void {
  }

}
