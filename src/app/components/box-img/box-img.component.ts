import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-box-img',
  templateUrl: './box-img.component.html',
  styleUrls: ['./box-img.component.css']
})
export class BoxImgComponent implements OnInit {
  
  @Input() img: string;
  @Input() text: string;

  constructor() { }

  ngOnInit(): void {
  }

}
