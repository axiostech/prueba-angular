import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

//modules
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { OurServicesComponent } from './components/our-services/our-services.component';
import { RecomendacionesComponent } from './components/recomendaciones/recomendaciones.component';
import { ZipCodeComponent } from './components/zip-code/zip-code.component';
import { HowWorkComponent } from './components/how-work/how-work.component';
import { BoxImgComponent } from './components/box-img/box-img.component';
import { FooterComponent } from './components/footer/footer.component';
import { JoinNowComponent } from './components/join-now/join-now.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    OurServicesComponent,
    RecomendacionesComponent,
    ZipCodeComponent,
    HowWorkComponent,
    BoxImgComponent,
    FooterComponent,
    JoinNowComponent,
    CarouselComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    HttpClientModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
